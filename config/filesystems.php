<?php
return [
    'media' => [
        'driver' => 'local',
        'root'   => public_path('media'),
    ],

    'media_private' => [
        'driver' => 'local',
        'root'   => storage_path('app/media'),
    ],

    'uploads' => [
        'driver' => 'local',
        'root'   => storage_path('uploads'),
    ],
    'spaces' => [
        'driver' => 's3',
        'key' => env('DO_SPACES_KEY'),
        'secret' => env('DO_SPACES_SECRET'),
        'endpoint' => env('DO_SPACES_ENDPOINT'),
        'region' => env('DO_SPACES_REGION'),
        'bucket' => env('DO_SPACES_BUCKET'),
        'ACL'    => 'public-read',
        'visibility' => 'public',
    ],
];
