<?php

namespace Cuatrokb\Media\Exceptions\Collections;

use Exception;

class MediaCollectionAlreadyDefined extends Exception {}
