<?php

Route::middleware(['web', 'auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::namespace('Cuatrokb\Media\Http\Controllers')->group(function () {
        Route::post('upload',                   'FileUploadController@upload')->name('cuatrokb/media::upload');
        Route::get('view',                      'FileViewController@view')->name('cuatrokb/media::view');
    });
});
